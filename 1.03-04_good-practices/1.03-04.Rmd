---
title: An R Markdown document converted from "1-04.ipynb"
output: html_document
---

```{r}
library(MASS) # to generate correlated data
library(pwr) # to calculate statistcal power
library(tidyverse) # for easy manipulations of dataframe and plotting with ggplot2

theme_set(theme_minimal())
```

```{r}
R.Version()$version.string
```


# Definitions

## Statistical power

The code below generates the figure that illustrates how statistical power manifests across many replications (slide #21).

```{r}

# Parameters for the simulations
r.list <- seq(0.1, 0.8, 0.1) # correlation coefficients for which data will be simulated
n.sample <- 20 # sample size of each replication
n.sims <- 40 # number of replications, for each value of correlation coefficient
n.sims.tot <- n.sims*length(r.list) # total number of simulations

# Initialization of output variables
p <- vector("numeric", n.sims.tot) # p-values
k <- 0 # global loop index

# Simulations
for (rr in r.list) {
  for (ss in 1:n.sims) {
    
    # Generate bivariate normal data with specified correlation
    data <- mvrnorm(n = n.sample, mu = c(0,0),
                    Sigma = matrix(c(1, rr, rr, 1), nrow = 2))
    
    # Run correlation test and save
    k <- k + 1
    p[k] <- cor.test(data[,1], data[,2])$p.value
  }
}

# Gather all information in a dataframe for plotting
df <- data.frame(p, significance = (p<.05),
                 r = rep(r.list, each = n.sims),
                 idx = 1:n.sims.tot) %>%
  group_by(r) %>% mutate(idx = row_number())

# Plot
ggplot(df, aes(x = idx, y = r, fill = significance, shape = significance)) +
  geom_point(alpha = 0.7, size = 2.5) +
  scale_shape_manual(values = list(`TRUE` = 21, `FALSE` = 23),
                     labels = c(`TRUE`="p < .05",`FALSE`="p > .05")) +
  scale_fill_manual(values = c(`TRUE` = "turquoise", `FALSE` = "firebrick1"),
                    labels = c(`TRUE`="p < .05",`FALSE`="p > .05")) +
  scale_x_continuous(breaks = scales::pretty_breaks(20)) +
  scale_y_continuous(breaks = r.list) +
  labs(x = "replication #", y = "true correlation coefficient", fill = "", shape = "") +
  theme(text = element_text(size = 20),
        legend.position = "top",
        panel.grid.minor.y = element_blank())

# Calculate true power for 4 values of correlation coefficient
pwr.r.test(n = n.sample, r = .15)
pwr.r.test(n = n.sample, r = .35)
pwr.r.test(n = n.sample, r = .55)
pwr.r.test(n = n.sample, r = .75)

```


## Effect sizes

The code below generates the figure that illustrates differences between relative and absolute effect sizes of associations (correlation and slope, resp.) between 2 variables (slide #22).

### Correlations

```{r}
n <- 200
r.list <- c(0.1, 0.45, 0.8)
df <- data.frame()

for (rr in r.list) {
  data <- mvrnorm(n = n, mu = c(0,0),
                  Sigma = matrix(c(1, rr, rr, 1), nrow = 2))
  
  df <- bind_rows(df, data.frame(x = data[,1], y = data[,2], r = rr))
}

df %>% 
  ggplot(aes(x, y, color = as.factor(r))) +
  facet_wrap(~ r, labeller = label_both) +
  geom_point() +
  geom_smooth(method = "lm") +
  coord_equal() +
  guides(color = "none", x = "none", y = "none") +
  theme(text = element_text(size = 20),
        panel.border = element_rect(fill=NA))
```


### Regression coefficients

```{r}

n <- 200
slope <- 1.2
sd.list <- c(1,1.8,3)
df <- data.frame()

for (std in sd.list) {
  x <- rnorm(200)
  y <- slope*x + rnorm(200, sd = std)
  r <- cor.test(x,y)$estimate
  
  df <- bind_rows(df, data.frame(x, y, r = round(r,2), std, slope))
}

df %>% 
  ggplot(aes(x, y, color = as.factor(r))) +
  facet_wrap(~ r + slope, labeller = label_both) +
  geom_point() +
  geom_smooth(method = "lm", color = "grey20") +
  coord_equal() +
  guides(color = "none") +#, x = "none", y = "none") +
  theme(text = element_text(size = 20),
        panel.border = element_rect(fill=NA))
  
```



# Calculating statistical power

The **pwr** library provides convenient functions for the most common, simple statistical models: t-test, Chi square, Pearson correlation, one-way Aonva.

It uses the formal relationship that can be expressed for simple models between the sample size, effect size, significance level and power. It can calculate any of these 4 variables based on the others.

## Example 1 (you need always more than what you think)

How many participants do I need for a **between-group** design with **2 conditions**, with a minimum interesting **effect size** (Cohen's *d*) of **0.2**, and a **statistical power of 90%**?

```{r}
pwr.t.test(d = 0.2, power = .9, sig.level = .05, type = "two.sample")
```

That's a lot! Let's be less ambitious. A statistical power of **80%**?

```{r}
pwr.t.test(d = 0.2, power = .8, sig.level = .05, type = "two.sample")
```

Still a lot... What if I use a **within-subject** design then?

```{r}
pwr.t.test(d = 0.2, power = .8, sig.level = .05, type = "paired")
```

## Example 2 (keep it simple)

How many participants do I need for a **between-subject** design with **4 conditions**, with an estimated **effect size** (Cohen's *f*) of **0.25**, and a **statistical power of 80%**?

```{r}
pwr.anova.test(k = 4, f = 0.25, power = .8, sig.level = .05)
```

```{r}
pwr.anova.test(k = 2, f = 0.2, power = .8, sig.level = .05)
# pwr.t.test(d = 0.5, power = .8, sig.level = .05)
```

## Example 2 (post hoc power - never do that!)

```{r}
pwr.r.test(r = 0.48, n = 27, sig.level = .05)
```

## EXERCISE

Calculate the statistical power of your own study, both for the expected effect size (e.g., based on the literature), and for a minimal interesting effect size.
Is your study properly powered?


## Pilot studies

The code below generates the figure illustrating why small pilot studies are unfit for estimation of effect sizes, a practice that is common in clinical research (slides #26 and #27).

```{r}

n.sample <- 10
n.studies <- 20
d <- 0.8

pwr.t.test(n = n.sample, d = d, sig.level = .05, type = "two.sample")

df = data.frame()

for (kk in 1:n.studies) {
  grpA <- rnorm(n.sample, mean = d, sd = 1)
  grpB <- rnorm(n.sample, mean = 0, sd = 1)
  
  tt <- t.test(grpA,grpB)
  df <- bind_rows(df, data.frame(estimate = tt$estimate[1]-tt$estimate[2],
                                 ci.inf = tt$conf.int[1],
                                 ci.sup = tt$conf.int[2]))
}

# Calculate the sample size that would be predicted based on the sample estimate at 80% power,
# as well as the true statistical power of the main trial based on that predicted sample size
df <- df %>% 
  mutate(n = map_dbl(estimate, .f = ~pwr.t.test(d = ., power = .8)$n),
         power.trial = map_dbl(n, .f = ~pwr.t.test(n = ., d = d)$power)) %>% 
  mutate(idx = row_number())
  
# Plot
df %>% 
  ggplot(aes(x = idx, y = estimate, color = ci.inf > 0)) +
  geom_hline(yintercept = 0) +
  geom_hline(yintercept = d, linetype = 2) +
  geom_point(size = 2.5) +
  geom_errorbar(aes(ymin = ci.inf, ymax = ci.sup), width = 0.3, size = 0.8) +
  geom_text(data = df %>% filter(ci.inf > 0),
            aes(label = paste0("n=",round(n),"\n(",round(100*power.trial),"%)")), 
            y = Inf, hjust = 0.5, vjust = 1, color = "grey20", size = 3, fontface = "bold", alpha = 1) +
  scale_x_continuous(breaks = 1:20) +
  scale_color_hue(labels = c(`TRUE`="proceed",`FALSE`="stop")) +
  expand_limits(y =c (-1,3.5)) +
  labs(x = "pilot study #", y = "sample estimate (95% CI)", color = "decision") +
  theme(text = element_text(size = 20),
        legend.position = "top",
        # panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank())
                               
```


## Post hoc (observed) power as a function of true power

Can we estimate the statistical power of a study based on its results (i.e., the reported effect size)? Let's see what we would obtain with many studies all with a true power of 30%.

```{r}

n <- 30
es <- 0.27
pwr.t.test(n = n, d = es, sig.level = .05, type = "one.sample")

N <- 1000
p = vector("numeric",N)
d = vector("numeric",N)
pwr = vector("numeric",N)
for (k in 1:N) {
    sim = simulate(n = 30, es = es, alpha = .05)
    p[k] = sim[1]
    d[k] = sim[2]
    pwr[k] = pwr.t.test(n = n, d = d[k], sig.level = .05, type = "two.sample")$power

}

hist(p)
hist(d)
hist(pwr)
mean(pwr)

plot(p,pwr, xlab = "p-value", ylab = "observed power")
```
The distribution of retrospective power, based on the sample estimate of effect size, is extremely wide! Even the average is wrong: 40% instead of 30%!

Let's see how this "power inflation" changes with true power:

```{r}
n <- 30

# Range of true effect sizes
pwr.list = seq(from = 0.1, to = 1, by = 0.05)

# Initialize the dataframe that will contain the results
df = data.frame()

N = 1000 # we decrease the number of simulations to speed up the calculations

for (pwr in pwr.list) {
  
  es <- pwr.t.test(n = n, power = pwr, sig.level = .05, type = "one.sample")$d

  p = vector("numeric",N)
  d = vector("numeric",N)
  pwr.obs = vector("numeric",N)
  for (k in 1:N) {
    sim = simulate(n = n, es = es, alpha = .05)
    p[k] = sim[1]
    d[k] = sim[2]
    pwr.obs[k] = pwr.t.test(n = n, d = d[k], sig.level = .05, type = "one.sample")$power
    
  }
  df <- bind_rows(df, data.frame(pwr.true = pwr, pwr.obs = pwr.obs, p.val = p, es = es, d.obs = d))
}
```


```{r}

# Averages
ggplot(df %>%
         group_by(pwr.true) %>%
         summarise(mean = mean(pwr.obs),
                   median = median(pwr.obs)) %>% 
         gather(key = average, value = pwr.obs, starts_with("me")),
       aes(y = pwr.obs, x = pwr.true, color = average)) +
geom_point() +
geom_line() +
geom_abline(slope = 1, linetype = 2, color = "grey50") +
annotate(geom = "text", label = "expected relationship", x = .80, y = .85, angle = 45, size = 3.5, color = "grey50") + 
scale_x_continuous(limits = c(0,1), breaks = scales::pretty_breaks(8), label = scales::label_percent(accuracy = 1)) +
scale_y_continuous(limits = c(0,1), breaks = scales::pretty_breaks(5), label = scales::label_percent()) +
coord_equal() +
labs(x = "True power", y = "Post hoc (observed) power")

# Full distributions
ggplot(df, aes(y = pwr.obs, x = as.factor(pwr.true))) +
geom_violin() +
geom_abline(slope = 1, linetype = 2, color = "grey50") +
scale_y_continuous(limits = c(0,1), breaks = scales::pretty_breaks(5), label = scales::label_percent()) +
coord_equal(ratio = 10) +
labs(x = "True power", y = "Post hoc (observed) power")

```


```{r}
# Calculate the "observed" aka "post hoc" statistical power
df.pwr.posthoc <- df %>% mutate(power_sim = pwr.t.test(n = 30, d = typeM*es, sig.level = .05)$power)

# Plot
ggplot(df.pwr.posthoc, aes(y = power_sim, x = power)) +
geom_point() +
geom_abline(slope = 1, linetype = 2, color = "grey50") +
annotate(geom = "text", label = "expected relationship", x = .70, y = .73, angle = 45, size = 3.5, color = "grey50") + 
scale_x_continuous(breaks = scales::pretty_breaks(8), label = scales::label_percent(accuracy = 1)) +
scale_y_continuous(breaks = scales::pretty_breaks(5), label = scales::label_percent()) +
coord_fixed() +
labs(x = "True power", y = "Post hoc (observed) power")
```

As expected, post hoc power overestimates true power (on average).
It looks like there is a rather straightforward, close to linear relationship between post hoc power and true power, suggesting we could infer (even approximately) the latter from the former. However, remember that we are plotting the average over many simulations. Actually, the variability is shockingly wide as demonstrated by Daniel Lakens (http://daniellakens.blogspot.com/2014/12/observed-power-and-what-to-do-if-your.html): observed power depends so much on the data that it can take pretty much any value, and is largely independent from true power.

## EXERCISE

For 3 different levels of statistical power (20%, 50% and 80%) simulate many experiments and visualize the distribution of post hoc statistical power.

Tip: use boxplots or violin plots.




# Consequences of low statistical power

## Low power invites significance-chasing

Quick-and-dirty code to generate the figures illustrating "statistical pareidolia"
```{r}

x <- rnorm(200)
y <- rnorm(200)

plot(x,y)
cor.test(x,y)

plot(x[1:20],y[1:20])
cor.test(x[1:20],y[1:20])

```

## Type M and S errors at low power

Let's first write a function that simulates 2 groups of normally distributed data with a certain effect size for the difference. It will be useful later for batch simulations.

```{r}

simulate <- function(n, es = 0.5, alpha = .05) {
    
    yA = rnorm(n, mean = es, sd = 1)
    yB = rnorm(n, mean = 0, sd = 1)
    
    p = t.test(yA, yB, paired = FALSE)$p.value
    d = (mean(yA) - mean(yB)) / 1
    
    return(c(p,d))
}
```

Let's now compare power estimated from simulations with theoretical powr
```{r}

n.sample <- 30
es = 0.1

## Power estimated from simulations

# Initialize the vectors that will store the results from the N simulated experiments
n.sims <- 5000
p <- vector("numeric", n.sims)
d <- vector("numeric", n.sims)

for (k in 1:n.sims) {
    # Calculate the p-value and observed effect size from one simulated experiment
    sim <- simulate(n = n.sample, es = es, alpha = .05)
    # Store results
    p[k] <- sim[1]
    d[k] <- sim[2]
}


# Effect size estimated from simulations (compare to the theoretical effect size "es")
cat("\nmean effect size in simulations = ", mean(d))


# Power estimated from simulations (compare to the theoretical value below)
cat("power from simulations = ", sum(p < .05)/n.sims)

## Theoretical power
pwr.t.test(n = n.sample, d = es, type = "two.sample")

```

Generate the figure that illustrates convergence of simulated samples' means towards the true effect size (slide #33, left).
```{r}

# - arrange results in a dataframe
df.plot <- data.frame(n = seq(n.sims), p = p, d = d) 

# - viualize the distribution of observed effect sizes
ggplot(df.plot, aes(x = d, y = ..count..)) +
  geom_histogram(fill = "grey70", bins = 20, boundary = 0) +
  geom_vline(xintercept = 0, color = "white") +
  geom_vline(xintercept = es, color = "grey20") +
  geom_vline(xintercept = es, color = "violet", linetype = 2) +
  annotate(geom = "text", label = "null ", x = 0, y = 0, hjust = 1, vjust = -0.5, color = "white") +
  annotate(geom = "text", label = " true effect size", x = es, y = Inf, hjust = 0, vjust = 1.5, color = "grey20") +
  annotate(geom = "text", label = "mean obs. effect size ", x = mean(d), y = Inf, hjust = 1, vjust = 1.5, color = "violet") +
  scale_x_continuous(breaks = seq(-1,1,0.2)) +
  labs(x = "effect size (Cohen's d)", y = "# of simulated studies")

```

Generate the figure that illustrates how publication bias breaks convergence of samples' means and lead to overestimation of effect sizes (slide #33, right).
```{r}
# - viualize the distribution of observed effect sizes as a function of test significance
es_signif = mean(filter(df.plot, p<.05)$d)

ggplot(df.plot, aes(x = d, fill = (p<.05))) +
  geom_histogram(position = "identity", alpha = 0.5, bins = 20, boundary = 0) +
  geom_vline(xintercept = es, color = "grey20") +
  geom_vline(xintercept = es_signif, color = "turquoise3", linetype = 2) +
  annotate(geom = "text", label = " true effect size", x = es, y = Inf, hjust = 0, vjust = 1.5) +
  annotate(geom = "text", label = " mean obs. effect size\n (signif. studies only)", x = es_signif, y = Inf, hjust = 0, vjust = 2, color = "turquoise3") +
  scale_fill_hue(labels = c(`TRUE`="p<.05",`FALSE`="p>.05")) +
  guides(fill = guide_legend(title = "", override.aes = list (alpha = 1))) +
  scale_x_continuous(breaks = seq(-1,1,0.2)) +
  labs(x = "effect size (Cohen's d)", y = "# of simulated studies") +
  theme(legend.position = c(0.12,.95))

### CONCLUSION
# When the statistical power is low, significant studies can greatly overestimate effect sizes, and can even get the sign of the effect wrong! Therefore publication bias (the fact that studies are more likely to get published if they have significant results) leads to an overestimation of effect sizes.
```

```{r}
# Type M error: ratio of effect size of statistically significant simulated experiments / true effet size

mean(abs(d[p<.05]))/es # 1 = no magnitude amplification
```

```{r}
# Type S error: proportion of statistically significant simulated experiments that get the sign of the effect wrong

sum((p<.05) & (d<0))/sum(p<.05) # !!!
```

## Type M and S errors as a function of power

How are type M and S errors affected by statistical power? Let's re-run the entire simulation and analysis procedure, but for a whole range of statistical power values. We will explore a range of power values by manipulating the corresponding effect size while keeping the sample size constant (but we could as well do the opposite). 

```{r}

# Range of power values
pwr.list = seq(from = 0.06, to = 1, by = 0.02)
n.sample <- 30
n.sims <- 1000 # we decrease the number of simulations to speed up the calculations

# Initialize the dataframe that will contain the results
df = data.frame(power = pwr.list, typeM = NA, typeS = NA)

for (pwr in pwr.list) {
    
  es <- pwr.t.test(n = n, power = pwr, sig.level = .05, type = "one.sample")$d
  
  p = vector("numeric", n.sims)
  d = vector("numeric", n.sims)
  
  for (k in 1:n.sims) {
    sim = simulate(n = 30, es = es, alpha = .05)
    p[k] = sim[1]
    d[k] = sim[2]
  }
  
  # True power
  df[df$power==pwr,"es"] = es
  
  # Type M error
  df[df$power==pwr,"typeM"] = mean(abs(d[p<.05]))/es
  
  # Type S error
  df[df$power==pwr,"typeS"] = sum((p<.05) & (d<0))/sum(p<.05)
  
}

df
```

Let's look at the results.

```{r, fig.width=3, fig.height=3}
ggplot(df, aes(y = typeM, x = power, group = )) +
  geom_hline(yintercept = 1, linetype = 2, color = "grey50") +
  geom_line() + 
  expand_limits(x = 0, y = 0) +
  scale_x_continuous(limits = c(0,1), breaks = scales::pretty_breaks(5), label = scales::percent) +
  scale_y_continuous(breaks = scales::pretty_breaks(5)) +
  labs(title = "Type M error",
       x = "Statistical power",
       y = "Magnitude amplitfication")

ggplot(df, aes(y = typeS, x = power, group = )) +
  geom_line() +
  scale_x_continuous(breaks = seq(.1,1.5,.1)) +
  labs(title = "Type S error", y = "Fraction of simulations getting the sign wrong")
```

*CONCLUSION:*
While type S error occurs virtually only in very low powered studies (power < 15%), effect sizes are overestimated in all studies with a power <90%, and it becomes a serious concern below 60-70%.



# Consequences of QRPs

## Simulation of situation C from Simmons, Nelson & Simonsohn 2012

The following situation reproduces **situation C** in Simmons, Nelson & Simonsohn 2012 (table 1, see also slides 28 and 29 in the course):
We model simulated data with or without a covariate, with or without interaction with the covariate, and we choose the "most significant" one. Remember that there are no effects whatsoever in the simulated data, and therefore any statistically significant outcome is a false positive.

```{r}
### Function that does step 1 (generate random data with no effect) and step 2 (exploit the analytic degrees-of-freedom and retain the best p-value)

simulate_qrpC = function(n = 30, alpha = .05) {
  
    # STEP 1
    y0 = rnorm(n)
    yT = rnorm(n)
    
    # We use a random binary variable as a covariate
    cov = as.integer(rbernoulli(n))
    # We could also use a uniform variable 
    # cov = runif(n, min = 18, max = 70)
    
    df = data.frame(id = seq(n), baseline = y0, treatment = yT, covariate = cov) %>%
        gather(key = condition, value = y, baseline, treatment)
    
    
    # STEP 2: calculate and store p-values from all possible modeling choices
    # Model without covariate
    p = t.test(y0,yT, paired = F)$p.value
    
    # Model with covariate
    m_cov = lm(y ~ covariate + condition, df)
    p_cov = Anova(m_cov)["condition","Pr(>F)"]
    
    # Model with covariate interacting with the experimental condition
    m_interact = lm(y ~ condition*covariate, df)
    # - Main effect of CONDITION
    p_interact1 = Anova(m_interact)["condition","Pr(>F)"]
    # - Interaction
    p_interact2 = Anova(m_interact)["condition:covariate","Pr(>F)"]
    
    return(c(p,p_cov,p_interact1,p_interact2))
}
```

```{r}
### Repeat steps 1 and 2 many times

N = 1000

p = vector("numeric",N)
p_qrp = vector("numeric",N)
for (k in 1:N) {
    p_sim = simulate_qrpC()
    p[k] = p_sim[1]
    p_qrp[k] = min(p_sim)
}
```

```{r}
### Estimate false positive rates based on simulations

sum(p < .05)/N # this one should be close to .05
sum(p_qrp < .05)/N # this one gives an idea of how much the QRP inflates the false positive rate
```

## EXERCISE

Write the code to simulate the other situations from Simmons, Nelson & Simonsohn 2012. Compare your results to theirs.


# Corrections for multiple comparison

## FWER

### Independent tests
```{r}

n.sample <- 30
n.H0 <- 900 
n.H1 <- 100

p = vector("numeric", n.H0)
for (k in 1:n.H0) {
  p[k] = t.test(rnorm(n.sample), rnorm(n.sample), paired = F)$p.value
}
df <- data.frame(p = p, H0 = TRUE)

p = vector("numeric", n.H1)
for (k in 1:n.H1) {
  p[k] = t.test(rnorm(n.sample), rnorm(n.sample, mean = 0.75), paired = F)$p.value
}
df <- bind_rows(df, data.frame(p = p, H0 = FALSE)) %>% 
  arrange(p) %>% mutate(idx = row_number())

df %>% filter(p<.05) %>% mutate(FDR = cumsum(H0)/idx)

df %>% 
  filter(p < .05) %>% 
  ggplot(aes(x = idx, y = as.numeric(!H0), color = as.factor(!H0))) +
  geom_vline(xintercept = 12.5, color = "grey50") +
  annotate(geom = "text", x = 12.5, y = Inf, hjust = 1, vjust = 1, label = "FWER \n(Bonferroni) ", color = "grey50", size=5, fontface = "bold") +
  geom_vline(xintercept = 27.5, color = "grey50", linetype = 2) +
  annotate(geom = "text", x = 27.5, y = Inf, hjust = 0, vjust = 1, label = " FDR=5%\n (true)", color = "grey50", size=5, fontface = "bold") +
  geom_vline(xintercept = 41.5, color = "grey50", linetype = 2) +
  annotate(geom = "text", x = 41.5, y = Inf, hjust = 0, vjust = 1, label = " FDR~5%\n (Benjamini-Hochberg)", color = "grey50", size=5, fontface = "bold") +
  geom_vline(xintercept = Inf, color = "grey50") +
  annotate(geom = "text", x = Inf, y = Inf, hjust = 1, vjust = 1, label = "p < .05 \nFDR=32% ", color = "grey50", size=5, fontface = "bold") +
  geom_point(shape = "|", size = 4) +
  scale_x_continuous(breaks = scales::pretty_breaks(6)) +
  scale_y_continuous(breaks = c(0,1), limits = c(-0.5,2), labels = c("False positive","True positive")) +
  guides(color = "none") +
  labs(x = "individual significant p-values (< .05) in increasing rank", y = "") +
  theme(text = element_text(size = 20)) -> g

(p.adjust(df$p, method="BH") < .05) %>% sum
(p.adjust(df$p, method="BY") < .05) %>% sum
(p.adjust(df$p, method="bonferroni") < .05) %>% sum
(p.adjust(df$p, method="holm") < .05) %>% sum
(p.adjust(df$p, method="hommel") < .05) %>% sum

```

### Positively associated```{r}

n.sample <- 30 # sample size for each test
rho <- 0.5 # correlation coefficient between dependent variables

n.H0 <- 50 
n.H1 <- 50

X <- rnorm(n.sample) # predictor
Y0 <- rnorm(n.sample) # dependent variable of reference

# Generate p-values of correlation tests between X and Y1, X and Y2, etc. where the Yi are correlated so as to create positive dependency, *under H0*
p = vector("numeric", n.H0)
for (k in 1:n.H0) {
  Z <- rnorm(n.sample) # standard normal variable independent of Y0
  Y <- rho*Y0 + sqrt(1 - rho^2)*Z # generate Y with specified correlation to Y0
  p[k] = cor.test(X,Y)$p.value
}
df <- data.frame(p = p, H0 = TRUE)

# Generate p-values of correlation tests between W and Y1, W and Y2, etc. where the Yi are correlated so as to create positive dependency, *under H1*
p = vector("numeric", n.H1)
for (k in 1:n.H1) {
  Z <- rnorm(n.sample) # standard normal variable independent of Y0
  Y <- rho*X + rho*Y0 + sqrt(1 - rho^2 - rho^2)*Z # generate Y with specified correlation to Y0 and predictor Z
  p[k] = cor.test(X,Y)$p.value
}
df <- bind_rows(df, data.frame(p = p, H0 = FALSE)) %>% 
  arrange(p) %>% mutate(idx = row_number())

(p.adjust(df$p, method="BH") < .05) %>% sum
(p.adjust(df$p, method="BY") < .05) %>% sum

(p.adjust(df$p, method="bonferroni") < .05) %>% sum
(p.adjust(df$p, method="holm") < .05) %>% sum
(p.adjust(df$p, method="hochberg") < .05) %>% sum
(p.adjust(df$p, method="hommel") < .05) %>% sum
```

