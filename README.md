------------------------------------------------------------------------

------------------------------------------------------------------------

**Table of Contents**

-   [Curriculum](#curriculum)
-   [Schedule](#schedule)
-   [Recommended reads](#recommended-reads)
-   [Contact](#contact)

------------------------------------------------------------------------

## Curriculum {#curriculum}

### Basics of statistics

**Session 1.1: Statistical inference I**\
- Motivations: statistics in scientific reasoning: population & sample, description & inference\
- Basic concepts: statistical model, probabilities, distributions, parameters\
- Estimation: population mean & variance, Maximum Likelihood Estimation (MLE)

**Session 1.2: Statistical inference II**\
- Measuring uncertainty with confidence intervals: computation & interpretation\
- Hypothesis testing: Fisher's procedure, interpretation of p-values\
- The significant problems of p-values

**Session 1.3: Good practices in statistics, part I**\
- The replication crisis: prevalence & factors\
- Reproducibility: statistical programming, version control, virtual environments\
- Type I & II error rates: definition, trade off\
- Standardized & absolute effect sizes\
- Statistical power: definition, computation, pitfalls

**Session 1.4: Good practices in statistics, part II**\
- False negatives: prevalence & consequences of low power, publication bias\
- False positives: questionable research practices, multiple comparison problem & correction\
- Pre-registration & registered reports\
- Open science practices: code & data sharing, FAIR principles, resources, BIDS\
- Reporting: guidelines & tools\
<!--#  - Positive predictive value (PPV), base rate of true hypotheses--> <!--# - Changing one's mindset: para-statistical thinking, exploratory research, pre-registration-->

**Session 1.5: Linear models, part I**\
- Causality, statistical dependence & correlation\
- Regressions & AN(C)OVAs as linear models\
- Model fitting: Ordinary Least Squares (OLS), assumptions, diagnostics

**Session 1.6: Linear models, part II**\
- Categorical predictors: contrast coding, ANOVA, omnibus tests\
- Interactions: significance testing, *post hoc* tests, simple effects\
- Common statistical tests as linear models\
- Extensions of the linear model: multilevel, generalized & non-linear models

### Advanced statistics

**Session 2.1: Mixed linear models, part I**\
- Motivation & concepts: non-independence, fixed & random effects, variance-covariance matrices\
- Model specification: syntax with lme4, practical exercise\
- Analysis: diagnostics, hypothesis testing, reporting

**Session 2.2: Mixed linear models II**\
- Generalized mixed models\
- Debugging: interpreting and fixing warnings and errors in lme4\
- Model selection, power analysis

**Session 2.3: Predictive statistics**\
- Motivation & concepts: inference vs. prediction, generalizability vs. interpretability\
- Criteria for model selection: goodness-of-fit, variance-bias tradeoff\
- Prediction: loss functions, performance metrics, cross-validation

**Session 2.4: Bayesian statistics, part I**\
- Motivations: frequentist vs. Bayesian paradigms\
- Fundamentals: Bayes theorem, prior & posterior probabilities\
- Bayesian statistical inference: point & interval estimates, conjugate distributions, MCMC sampling

**Session 2.5: Bayesian statistics, part II**\
- Specifying the priors: Bayesian subjectivity, (non-)informative priors\
- Hypothesis testing; posterior-based approach, Bayes factors\
- R implementations: the ecosystem of Bayesian packages + a few simple code examples

**Session 2.6: Bayesian statistics, part III**\
- Practical Bayesian modelling with rstanarm: specification, diagnostics, estimation, hypothesis testing\
- Other common software: brms (R), JASP\
- Going further: prior & posterior predictive checks, model sensitivity

## Schedule {#schedule}

### Basics of statistics

October - November 2023.

```{=html}
<!--#
# \| Session \| Title \| Date \| Time \| Location \|

# \| :--- \| :--- \| :--- \| :--- \| :--- \|

# \| Session 1.1 \| Statistical inference I \| Tuesday, Oct 24th \| 9:30 to 12:30 \| ISC amphitheather \|

# \| Session 1.2 \| Statistical inference II \| Wednesday, Oct 25th \| 9:30 to 12:30 \| ISC amphitheather \|

# \| Session 1.3 \| Good practices in statistics I \| Thursday, Oct 26th \| 14:00 to 17:00 \| ISC amphitheather \|

# \| Session 1.4 \| Good practices in statistics II \| Tuesday, Oct 31st \| 9:30 to 12:30 \| ISC amphitheather \|

# \| Session 1.5 \| Linear models I \| Thursday, Nov 2nd \| 14:00 to 17:00 \| ISC amphitheather \|

# \| Session 1.6 \| Linear models II \| Friday, Nov 3rd \| 9:30 to 12:30 \| ISC amphitheather \|
-->
```
### Advanced statistics

| Session     | Title                   | Date                | Time              | Location                      |
|:-------------|:-------------|:-------------|:-------------|:---------------|
| Session 2.1 | Multilevel models I     | Tuesday, Dec 5th    | 9:30 to 12:30     | ISC amphitheather             |
| Session 2.2 | Multilevel models II    | Wednesday, Dec 6th  | 9:30 to 12:30     | ISC amphitheather             |
| Session 2.3 | Predictive statistics   | Thursday, Dec 7th   | 14:00 to 17:00    | ISC amphitheather             |
| Session 2.4 | Bayesian statistics I   | Tuesday, Dec 12th   | 9:30 to 12:30 | **NeuroCampus** amphitheather |
| Session 2.5 | Bayesian statistics II  | Wednesday, Dec 13th | 9:30 to 12:30     | **NeuroCampus** amphitheather |
| Session 2.6 | Bayesian statistics III | Thursday, Dec 14th  | 14:00 to 17:00    | **Dycog** library             |

### Practical sessions

| Session       | Date          | Time          | Location   |
|:--------------|:--------------|:--------------|:-----------|
| Data analysis | to be defined | to be defined | **online** |

```{=html}
<!--
| Learning R, part I  | Wednesday, Oct 25th | 14:00 to 17:00 | **NeuroCampus, room F28** |
| Learning R, part II | Tuesday, Nov 7th    | 09:30 to 12:30 | **NeuroCampus, room F28** |
-->
```
## Recommended reads {#recommended-reads}

In preparation.

## Contact {#contact}

If you have any question on the material or if you are interested in arranging a course, write to:

[oussama.abdoun\@pm.me](mailto:oussama.abdoun@pm.me){.email}
