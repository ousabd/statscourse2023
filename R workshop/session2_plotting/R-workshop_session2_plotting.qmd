---
title: "Learning R workshop"
subtitle: "Session2: data visualization"
author: "Oussama Abdoun"
date: today
date-format: long
format:
  html:
    toc: true
    toc-depth: 2
    toc-expand: true
    warning: false
    fig-width: 4
    fig-height: 3
editor: visual
---

# Prepare

## Libraries

Base R includes powerful and flexible plotting functions, but we are going to use `ggplot2`. The `ggplot2` package is optimized to work with dataframes and has a much more natural and pleasant syntax. `ggplot2` is inluded in the `tidyverse` ecosystem so there is no need to load it.

```{r}
library(tidyverse) # includes ggplot2
```

## Load data

We are going to use data from a publication entitled "*Ready to help, no matter what you did: responsibility attribution does not influence compassion in expert Buddhist practitioners"* (in press, [preprint here](https://doi.org/10.31234/osf.io/d6y4w)). You can find a short introduction to the scientific background, methodology and objects of the paper in the file `design.docx`.

```{r}
df <- read_csv("Data_Fucci_et_al_CAAT_self_reports.csv")
df # have a peak at the dataframe
```

## Preprocessing

Let's prepare data for visualization. The dataframe is already **tidy**. We will focus on Emotional videos only, and keep only the necessary columns. These steps are not mandatory but for clarity only.

```{r}

df.plot <- df %>%
  filter(StimType == "Emotional") %>% # keep only emotional videos
  select(Subject, Group, Condition, Context, Valence, Arousal, Empathy, Responsibility, Deservingness, Help) # keep only necessary columns

df.plot
```

# Visualization

## Data & mapping

We start by providing `ggplot()` with the **dataframe** and the **mapping** between data columns and visual features. We get an empty figure because we have not specified any geom yet, but see how the **axes** are already set up to receive discrete data on the x-axis, and continuous data on the y-axis.

```{r}
ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group))
```

## Distributional geoms

Let's start with **boxplots**.

```{r}

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  geom_boxplot()

```

We get something that definitely looks like what we expected!\
But it's hard to compare the cells because the boxplots only indicate the medians, and in the context of discrete scales like those used for self-reports, the medians are necessarily integers only.\
Let's see if **full distributions**, visualized with **violins**, are more informative.

```{r}

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  geom_violin()

```

The "wavy" shape of violins reflect the discrete nature of the self-report responses. But it doesn't really help to compare the locations of the cells. For this we need to explicitly **plot the means**.

## Summary statistics

Means are summary statistics. There are two ways to plot summarise statistics in `ggplot2`:

-   calculate them before plotting them

-   let `ggplot2` do everything (calculation + plotting)

```{r}
#| warning: false
ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  geom_violin() +
  stat_summary()

```

Cool, we can see the means now but `ggplot2` did not align them properly with the violin geoms! That's because there are 2 means (one for Novices + one for Experts) for each Context, so they end up on the sample horizontal position.

\
That was already the case for the violins, so why didn't we end up with two superimposed violins at the same horizontal position? That's because `ggplot2` has been programmed to avoid this behavior that would be unwanted 99% of the time. It has the appropriate default setting of "**dodging**" two overlapping distributions. This is not the case for `stat_summary()` but we can easily force it.

## Adjustment of positions

The extent to which points are dodged is set by the argument `width` of the function `position_dodge()`.

```{r}

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  geom_violin() +
  stat_summary(position = position_dodge(width = 1))

```

It's not clear though how to set the dodging of the points so that they align with the centers of violins, and that's because we don't know how much `ggplot2` dogded the violins. An easy solution would be to **specify manually the dodging** of all elements and to set them equal.

```{r}
# Let us specify a global "position dodge width" to apply to all elements. By doing so, we can easily tweak its value by changing a single value. 
pdw <- 1

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  geom_violin(position = position_dodge(width = pdw)) +
  stat_summary(position = position_dodge(width = pdw))

```

## Facetting

So far we have restricted ourselves to two factors: Group and Context. We have ignored Condition, for which we could make separate subplots. This is called **facetting** in `ggplot2`.

```{r}

pdw <- 1

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  facet_grid(~ Condition) +
  geom_violin(position = position_dodge(width = pdw)) +
  stat_summary(position = position_dodge(width = pdw))

```

Facetting functions use the tilde-based syntax reminiscent of the R syntax for statistical models. This comparison is useful to remember which side of the tilde (\~) map onto what:

-   the variable on the **right** will be used for **column subplots**, just like the **X** variable is plotted on the **horizontal axis**

-   the variable on the **left** will be used for **row subplots**, just like the **Y** variable is plotted on the **vertical axis**

What if we have **a lot of facets**, e.g. if we look at individual data?

Let's try.

```{r}
#| fig-width: 8

pdw <- 1

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  facet_grid(~ Subject) +
  geom_violin(position = position_dodge(width = pdw)) +
  stat_summary(position = position_dodge(width = pdw))
```

It looks like `ggplot2` has tried to display all participants one a single row... Consistent with what we *asked* it to do, but not what we *meant*!

Fortunately there is another facetting function implementing a different approach, `facet_wrap()`:

```{r}
#| fig-height: 12
pdw <- 1

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  facet_wrap(~ Subject, ncol = 5) +
  geom_violin(position = position_dodge(width = pdw)) +
  stat_summary(position = position_dodge(width = pdw))
```

## Theming

As far as the content is concerned, it seems like we are done. But the figure does not look quite ready for publication, there are a few esthetic changes that could be made:

-   a **white background** (instead of the current grey one)

-   **1-to-9 labels** on the y-axis to reflect the discrete nature of the response

-   remove the minor **grid lines on the y-axis**, which do not correspond to any possible response value

-   remove the **grid lines on the x-axis**, which are simply not necessary

```{r}
pwd <- 1

ggplot(data = df.plot,
       mapping = aes(x = Context, y = Empathy, color = Group)) +
  facet_grid( ~ Condition) +
  geom_violin(position = position_dodge(width = pwd)) +
  stat_summary(geom = "pointrange", position = position_dodge(width = pwd)) +
  scale_y_continuous(breaks = 1:9) +
  theme(panel.background = element_rect(fill = "white"),
        panel.grid.major.x = element_blank(),
        panel.grid.minor.y = element_blank(),
        panel.grid.major.y = element_line(color = "grey80"))
```

## 
